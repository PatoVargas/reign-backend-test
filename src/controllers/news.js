import { getAllNewsService, deleteNewService } from '../services/news'

export const getAllNews = async (req, res) => {
  try {
    const news = await getAllNewsService()

    res.status(200).send({
      news,
      error: false,
    })
  } catch (e) {
    res.status(500).send({
      message: e.message,
      error: true,
    })
  }
}

export const deleteNew = async (req, res) => {
  const { body } = req
  try {
    const response = await deleteNewService(body)
    if (response.ok === 1) {
      res.status(200).send({
        error: false,
      })
    } else {
      res.status(500).send({
        message: 'Error in delete hackerNew',
        error: true,
      })
    }
  } catch (e) {
    res.status(500).send({
      message: e.message,
      error: true,
    })
  }
}
