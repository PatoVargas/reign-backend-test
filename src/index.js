import express from 'express'
import dotenv from 'dotenv'

import { job, populateMongo } from './jobs/hackerNewsGetter'

import mongooseConfig from './config/mongo'

import newsRouter from './routes/news'
import healthRouter from './routes/health'

const cors = require('cors')
const bodyParser = require('body-parser')

dotenv.config()

mongooseConfig().then(() => {
  populateMongo()
})

job.schedule()

const app = express()

app.use(cors())
const jsonParser = bodyParser.json()

const port = process.env.PORT

app.use('/news', jsonParser, newsRouter)
app.use('/health', healthRouter)

app.get('/', (req, res) => {
  res.status(200).send('Welcome to reign backend test')
})

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`)
})

export default app
