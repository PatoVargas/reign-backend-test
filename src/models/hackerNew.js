import { Schema as _Schema, model } from 'mongoose'

const Schema = _Schema

const hackerNewSchema = new Schema({
  created_at: String,
  title: String,
  url: String,
  author: String,
  points: String,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  objectID: {
    type: String,
    unique: true,
  },
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  story_id: Number,
  deleted: Boolean,
})

const HackerNew = model('Model', hackerNewSchema, 'HackerNew')

export default HackerNew
