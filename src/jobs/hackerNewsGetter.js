import { scheduleJob, RecurrenceRule } from 'node-schedule'
import axios from 'axios'

import HackerNew from '../models/hackerNew'

const rule = new RecurrenceRule()
rule.minute = 0

const countHackerNewExists = async (id) => {
  const count = await HackerNew.find({ objectID: id }).countDocuments().exec()
  return count
}

export const populateMongo = async () => {
  const res = await axios.get(process.env.HACKER_URL)
  if (res.status === 200) {
    res.data.hits.forEach(async (element) => {
      const existHackerNew = await countHackerNewExists(element.objectID)
      if (existHackerNew !== 0) console.log('Element already exists')
      else HackerNew.create({ ...element })
    })
  }
}

export const job = scheduleJob(rule, async () => {
  await populateMongo()
})
