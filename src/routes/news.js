import { Router } from 'express'
import { getAllNews, deleteNew } from '../controllers/news'

const router = Router()

router.get('', getAllNews)
router.delete('', deleteNew)

export default router
