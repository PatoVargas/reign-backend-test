import HackerNew from '../models/hackerNew'

export const getAllNewsService = async () => {
  try {
    const allNews = await HackerNew.find({
      deleted: { $ne: true },
    }).sort('-created_at')
    return allNews
  } catch (error) {
    throw new Error('Internal error getting news')
  }
}

export const deleteNewService = async (data) => {
  try {
    const queryMongo = await HackerNew.updateOne(
      {
        // eslint-disable-next-line no-underscore-dangle
        _id: data._id,
      },
      {
        $set: {
          deleted: true,
        },
      },
      (err) => {
        if (err) return { ok: 0, message: 'mongo error' }
      },
    )
    return queryMongo
  } catch (error) {
    throw new Error('Internal error in delete new')
  }
}
