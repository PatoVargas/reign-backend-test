import { connect } from 'mongoose'

const mongooseConfig = async () => {
  const mongoUri = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DATABASE}`

  await connect(mongoUri, { useNewUrlParser: true })
  console.log('Success mongo connection')
}

export default mongooseConfig
