# REIGN TEST BACKEND

This is NodeJs/Express app created to a technical test in reign software factory. To run this project make sure you have docker and docker compose installed. 

## How run app

Create .env file in root dir project with the next vars (When use docker select MONGO_HOST=db, if not use docker use MONGO_HOST=localhost)

```
ENV=local
PORT=8080
HACKER_URL=https://hn.algolia.com/api/v1/search_by_date?query=nodejs

# MONGO_HOST=db
MONGO_HOST=localhost
MONGO_PORT=27017
MONGO_USER=
MONGO_PASSWORD=
MONGO_DATABASE=reign-test
```

### Docker

```
docker-compose build
docker-compose up 
```

### Without docker

```
npm install
npm start
```

## How run tests with coverage

```
npm run test:coverage
```

## How run tests without coverage

```
npm test
```