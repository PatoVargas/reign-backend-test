module.exports = {
  testEnvironment: 'node',
  setupFilesAfterEnv: ['./test/integration/supertest.js'],
  collectCoverageFrom: ['src/**/*.js', '!src/server.js', '!src/index.js'],
  coverageDirectory: 'coverage-int',
  coverageReporters: ['json', 'text'],
}
