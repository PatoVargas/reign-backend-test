module.exports = {
  testEnvironment: 'node',
  collectCoverageFrom: ['src/**/*.js', '!src/server.js', '!src/index.js'],
  coverageDirectory: 'coverage-unit',
  coverageReporters: ['json', 'text'],
}
