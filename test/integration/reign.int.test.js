import * as request from 'supertest';
import app from '../../src/index';

describe('Test Reign Backend', () => {
  const agent = request.agent(app);

  it('get news', async () => {

    const response = await agent.get(`/news`);

    expect(response.status).toBe(200);
    expect(response.body).not.toBeUndefined();
  });

  it('delete fail, document not exists', async () => {

    const response = await agent
      .delete(`/news`)
      .send({_id : "12312123213" });

    expect(response.status).toBe(500);
  });

  it('delete ok, document exists', async () => {

    const responseGet = await agent.get(`/news`);
    const response = await agent
      .delete(`/news`)
      .send({_id : responseGet.body.news[0]._id });

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({
      error: false,
    });
  });

  it('get status', async () => {
    const response = await agent.get('/health');

    expect(response.status).toBe(200);
    expect(response.body).toMatchObject({
      message: 'ok',
    });
  });

});
