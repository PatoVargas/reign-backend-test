/**
 * Merges different coverage reports. Using this solution:
 * https://github.com/facebook/jest/issues/2418#issuecomment-423806659
 *
 * The disabled rules are because some files will be created while running the coverage scripts
 */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable import/no-unresolved */
const libCoverage = require('istanbul-lib-coverage');
const { createReporter } = require('istanbul-api');

const integrationCoverage = require('../../coverage-int/coverage-final.json');
const unitCoverage = require('../../coverage-unit/coverage-final.json');

const normalizeJestCoverage = (obj) => {
  const result = obj;
  Object.entries(result).forEach(([k, v]) => {
    if (v.data) result[k] = v.data;
  });
  return result;
};

const map = libCoverage.createCoverageMap();
map.merge(normalizeJestCoverage(integrationCoverage));
map.merge(normalizeJestCoverage(unitCoverage));

const reporter = createReporter();
reporter.addAll(['json', 'text']);
reporter.write(map);
